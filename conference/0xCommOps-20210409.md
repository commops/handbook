# 2021年4月9日首次会议相关议程

## 主要的坑简介
* [开源社区运营手册](../README.md#目录)
* 开源“米其林”

## 社区运作/治理方式
### 社区治理模式
1. 可参考目前在[README中的治理章节](../README.md#治理)中的提议

### 社区运作
1. 可参考目前在[README的开发章节](../README.md#开发)中的提议
2. 在里程碑中，考虑使用文档松(*Docathon*)的形式组织开发，可以用线上或者线下的形式

### ToDo
1. 务必加入邮件列表[邮件列表](https://lists.openatom.io/hyperkitty/list/commops@openatom.io/)
2. 可以使用[Slack](https://join.slack.com/t/0xcommops/shared_invite/zt-ojxo7apd-izkMc3G3Z0t3AjdeKQls7g)等方式进行即时讨论
3. 确定4月份的里程碑1的首次会议

### 与会者建议
1. 姜宁：Call For Help需要一个指引文档，如何很快的贡献
2. 庄表伟：需要一份分角色的路线图，类似于游戏的进入目录；可以把知乎利用起来
3. 金耀辉：可以松里程碑式的项目管理，可以不追求完备度很强，注重过程的经验积累。积木式搭建，不要大而全过于重
4. 总结不要做什么也很重要，举一些具体的例子，正反面都需要
